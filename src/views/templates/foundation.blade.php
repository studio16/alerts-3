<div data-alert class="alert-box {{{ $alert_type }}} radius">
	{{{ $alert_text }}}
	<a href="#" class="close">&times;</a>
</div>